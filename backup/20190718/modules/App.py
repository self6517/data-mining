from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_mysqldb import MySQL
import re

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'db_skripsi'

mysql = MySQL(app);

@app.route('/')
@app.route('/<task>')
def home(task=''):
	data_db = None
	if task == '' :
		data_db = ''
	elif task == 'dm_kata_baku':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from tb_katadasar order by id_ktdasar desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
	elif task == 'dm_kalimat_hasil':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from process_kalimat order by Id desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
	elif task == 'dm_kata_negatif':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from m_kata_negatif order by id_kata_negatif desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
	elif task == 'dm_kata_gaul_asing':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from m_kata_gaul_asing order by id_kata_gaul desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
	elif task == 'input_kalimat':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from t_kalimat order by id desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
	else:
		data_db = ''
	return render_template("layout.html", task=task, d=data_db)

@app.route('/insert', methods = ['POST'])
def insert():
	if request.method == "POST":
		kata_dasar = request.form['kata_dasar']
		tipe_kata = request.form['tipe_kata']

		cur = mysql.connection.cursor()
		cur.execute("INSERT INTO tb_katadasar (katadasar, tipe_katadasar) VALUES (%s, %s)", (kata_dasar, tipe_kata))
		mysql.connection.commit()
		return redirect(url_for('home', task='dm_kata_baku'))



@app.route('/sentence', methods = ['POST'])
def sentence():
	if request.method == 'POST':
		data = {}
		sentence = request.form['sentence']

		data.update({"sentence":sentence})
		sentenceToLower = sentence.lower()
		data.update({"sentenceToLower" : sentenceToLower})
		modifSentence = ''
		for k in sentenceToLower.split("\n") :
			modifSentence = re.sub(r"[^a-zA-Z0-9]+", ' ', k)
			data.update({"modifSentence" : modifSentence})

			arrmodified = modifSentence.split(' ')
			data.update({"arrmodified" : arrmodified})
			
			type = ""
			labeled = ""
			if "tidak" in arrmodified :
				type = "negatif"
				labeled = "label label-danger"
			else:
				type = "positif"
				labeled = "label label-success"	
			
			data.update({
				"type" : type,
				"labeled" : labeled
			})

			return jsonify(data)





if __name__ == "__main__":
    app.run(debug=True)