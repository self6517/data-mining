function show_hide_cg(arr, e, base_url) {
  var doocgval = $(e).val();

  // hide
  var arrhide = arr['cs'];
  $.each(arrhide, function(key, value) {
    $('.'+value).parent().parent().css("display","none");
    $('.'+value).parent().parent().next('hr').css("display", "none");
  })

  // show
  var arrshow = arr['config'][doocgval]; 
  $.each(arrshow, function(key, value) {
    $('.'+value).parent().parent().css("display","block");
    $('.'+value).parent().parent().next('hr').css("display", "none");
  })

}

function show_hide_multi_select_cg(arr, e, base_url) {
  // hide
  var arrhide = arr['cs'];
  $.each(arrhide, function(key, value) {
    $('.'+value).parent().parent().css("display","none");
  })

  var show;
  var test2;

  // show
   $(e).find('option:selected').each(function(key, value) {
    test2 = $(this).val();
    show = arr['config'][test2];
    $.each(show, function(i, v) {
    $('.'+v).parent().parent().css("display","block");
    })
  })
}


function dsb_field_pnmr_cg(arr, e, base_url) {  
  var doocgval = $(e).val();
  
  var prm = 'false';
  if(doocgval == arr['trigger']) {
    prm = 'true';
  }
  $.each(arr['cfg'][prm], function(key, value) {
    $(e).parent().parent().find('.'+key).html('');
    $(e).parent().parent().find('.'+key).val('');
    if(value == 'disabled') {
      $(e).parent().parent().find('.'+key).addClass('dsb_click');
      $(e).parent().parent().find('.'+key).attr('required', false);
    } else {
      $(e).parent().parent().find('.'+key).removeClass('dsb_click');
      $(e).parent().parent().find('.'+key).attr('required', true);
    }
  })

  make_dsbclick_slc();
  make_slc_hover();
}

function dsb_field_komit_cg(arr, e, base_url) {  
  var doocgval = $(e).val();

  var prm = 'false';
  if(doocgval == arr['trigger']) {
    prm = 'true';
  }
  $.each(arr['cfg'][prm], function(key, value) {
    var newval = $(e).parent().parent().find('.'+key+' option:not([disabled]):first').val();
    $(e).parent().parent().find('.'+key).val(newval);
    $(e).parent().parent().find('.'+key).trigger('change');
    if(value == 'disabled') {
      $(e).parent().parent().find('.'+key).addClass('dsb_click');
      $(e).parent().parent().find('.'+key).attr('required', false);
    } else {
      $(e).parent().parent().find('.'+key).removeClass('dsb_click');
      $(e).parent().parent().find('.'+key).attr('required', true);
    }
  })
  make_dsbclick_slc();
  make_slc_hover();
}

function dsb_field_komit2_cg(arr, e, base_url) {  
  var doocgval = $(e).val();
  // console.log('res');
  var prm = 'false';
  if(doocgval == arr['trigger']) {
    prm = 'true';
  }
  $.each(arr['cfg'][prm], function(key, value) {
    // option destroy select
    if(value['option'] !== undefined) {
      $(e).parent().parent().find('.'+key).select2('destroy');
    }

    // dsb_click
    if(value['status'] == 'disabled') {
      $(e).parent().parent().find('.'+key).addClass('dsb_click');
      $(e).parent().parent().find('.'+key).attr('required', false);
    } else {
      $(e).parent().parent().find('.'+key).removeClass('dsb_click');
      $(e).parent().parent().find('.'+key).attr('required', true);
    }

    // option reinit select
    if(value['option'] !== undefined) {
      $.each(value['option'], function(key2, value2) {
        if(value2 == 'show') {
          $(e).parent().parent().find('.'+key+' option[value="'+key2+'"]').attr('disabled', false);
        } else {
          $(e).parent().parent().find('.'+key+' option[value="'+key2+'"]').attr('disabled', true);
        }
      })

      // reinit select2 
      $(e).parent().parent().find('.'+key).select2({width: '100%'}); 
      var newval = $(e).parent().parent().find('.'+key+' option:not([disabled]):first').val();
      $(e).parent().parent().find('.'+key).val(newval);
      $(e).parent().parent().find('.'+key).trigger('change');   
    }
  })
  make_dsbclick_slc();
  make_slc_hover();
}
