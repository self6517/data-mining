function vld_mekanisme_ulo_dsb(arr, i, doorval, base_url) {
  var jml  = $(document).find('.'+arr['from']).length;
  
  for(i2=0;i2<jml;i2++) {
    var fromVal  = $(document).find('.'+arr['from']+':eq('+i2+')').val();
    var fromStr  = arr['from'];
    var ckpwil   = fromStr.replace("mek_ulo", "ckpwil");

    if(fromVal == 'Self Assesment') {
      // $(document).find('.'+arr['target']+':eq('+i+')').addClass('dsb_click');
      // $(document).find('.'+arr['target']+':eq('+i+')').css('display', 'none');
      $(document).find('.'+arr['target']+':eq('+i2+')').addClass('dsb_click2');
      $(document).find('.'+arr['target']+':eq('+i2+')').attr('required', false);
    } else if (fromVal == 'Non ULO') {
      // $(document).find('.'+arr['target']+':eq('+i+')').addClass('dsb_click');
      // $(document).find('.'+arr['target']+':eq('+i+')').css('display', 'none');
      $(document).find('.'+arr['target']+':eq('+i2+')').addClass('dsb_click2');
      $(document).find('.'+arr['target']+':eq('+i2+')').attr('required', false);

      $(document).find('.'+arr['target2']+':eq('+i2+')').addClass('dsb_click2');
      $(document).find('.'+arr['target2']+':eq('+i2+')').attr('required', false);

      // $(document).find('.'+ckpwil+':eq('+i2+')').addClass('dsb_click2');
      $(document).find('.'+ckpwil+':eq('+i2+')').attr('required', false);  

      if(arr['target4'] !== undefined) {
        $(document).find('.'+arr['target4']+':eq('+i2+')').addClass('dsb_click2');
        $(document).find('.'+arr['target4']+':eq('+i2+')').attr('required', false);  
      }

    } else {
      // $(document).find('.'+arr['target2']+':eq('+i+')').css('display', 'none');
      $(document).find('.'+arr['target2']+':eq('+i2+')').addClass('dsb_click2');
      $(document).find('.'+arr['target2']+':eq('+i2+')').attr('required', false);
    }
  }
}

function vld_surat_ket_bi(arr, i, doorval, base_url) {
  $.ajax({
      type: "POST",
      url: base_url+'permohonan/vld_do/vld_surat_ket_bi',
      data: {kode_formula : arr['kode_formula']},
      datatype: "json",
      success: function(e) {
        var d = JSON.parse(e);
        if(d == true) {
          var span = $(document).find('.'+arr['target']).parent().parent().find('span');
          var newtext = span.text() + ' <i class="text-danger fa fa-asterisk"></i>';
          $(document).find('.'+arr['target']).parent().parent().find('span').html(newtext);
          $(document).find('.'+arr['target']).attr('required', true);
        } else {
          var row = $(document).find('.'+arr['target']).parent().parent().css('display', 'none');
      $(document).find('.'+arr['target']).attr('required', false);
        }
      }
  })
}

function rdy_prkt_fungsi(arr, i, doorval, base_url) {
  $.each(arr['value'], function(key, value) {
    $(document).find('.'+arr['target']+':not(:disabled):eq('+key+')').val(value).change();
    $(document).find('.'+arr['target']+':not(:disabled):eq('+key+')').addClass('dsb_click');
  })
}


function show_hide_rdy_old(arr, i, doorval, base_url) {
  // hide
  var arrhide = arr['cs'];
  $.each(arrhide, function(key, value) {
    $('.'+value).parent().parent().css("display","none");
    $('.'+value).parent().parent().next('hr').css("display", "none");
  })

  // show
  var arrshow = arr['config'][doorval]; 
  $.each(arrshow, function(key, value) {
    $('.'+value).parent().parent().css("display","block");
    $('.'+value).parent().parent().next('hr').css("display", "block");
  })
}


function show_hide_rdy(arr) {
  // hide
  var arrhide = arr['cs'];
  $.each(arrhide, function(key, value) {
    $('.'+value).parent().parent().css("display","none");
    $('.'+value).parent().parent().next('hr').css("display", "none");
  })

  // show
  var arrshow = $('.'+arr['from']).val();  
  if(Array.isArray(arrshow)) {
    $.each(arrshow, function(key, value) {
      $('.'+arr['cs'][value]).parent().parent().css("display","block");
      $('.'+arr['cs'][value]).parent().parent().next('hr').css("display","block");
    })
  } else {
    $('.'+arr['cs'][arrshow]).parent().parent().css("display","block");
    $('.'+arr['cs'][arrshow]).parent().parent().next('hr').css("display","block");
  }
}

function show_hide_rdy_multi(arr) {
  // hide
  var arrhide = arr['cs'];
  $.each(arrhide, function(k, v) {
    $.each(v, function(k2, v2) {
      $('.'+v2).parent().parent().css("display","none");
      $('.'+v2).parent().parent().next('hr').css("display", "none");
    })
  })
  
  // show
  var arrshow = $('.'+arr['from']).val();  
  $.each(arrshow, function(k, v) {
    if(arr['cs'][v] !== undefined) {
      $.each(arr['cs'][v], function(k2, v2) {
        $('.'+v2).parent().parent().css("display","block");
        $('.'+v2).parent().parent().next('hr').css("display","block");
      })
    }
  })
}

function get_data_source(arr, i, doorval, base_url, e) {
  if (arr['type'] == '') {
    arr['type'] = 'none';
  }
  $.ajax({
    type: "POST",
    url: base_url+'permohonan_izin/vld_do/get_data_source/'+arr['type']+'/'+arr['func'],
    data: {id_permohonan : arr['id_permohonan'], value : arr['value']},
    datatype: "json",
    success: function(e) {
      var ej = JSON.parse(e);
      if(arr['type'] == 'tbl') {
        $('.'+arr['target']).find('tbody').html(ej.data);
      } else if(arr['type'] == 'select') {
        $('.'+arr['target']).html(ej.option);
        $('.'+arr['target']).parent().append(ej.inp);
      }

      if(arr['funcAf'] != '') {
        window[arr['funcAf']['func']](arr['funcAf']['value']); 
      }
    },
    async: false
  })

  // dsb_tbl_aksi
  dsb_tbl_aksi2(e);
  if(arr['insert'] !== undefined) {
    $(e).removeAttr('name');
  }
}

function dsb_field_pnmr(arr, i, doorval, base_url) {
  var jml  = $(document).find('.'+arr['from']).length;
  
  // alert(jml);  
  for(i2=0;i2<jml;i2++) {
    var fromVal  = $(document).find('.'+arr['from']+':eq('+i2+')').val();
    if(fromVal == arr['trigger']) {
      $.each(arr['cfg']['true'], function(key, value) {
        if(value == 'disabled') {
          $(document).find('.'+key+':eq('+i2+')').addClass('dsb_click');
          $(document).find('.'+key+':eq('+i2+')').attr('required', false);
        } else {
          $(document).find('.'+key+':eq('+i2+')').removeClass('dsb_click');
          $(document).find('.'+key+':eq('+i2+')').attr('required', true);
        }
      })
    } else {
      $.each(arr['cfg']['false'], function(key, value) {
        if(value == 'disabled') {
          $(document).find('.'+key+':eq('+i2+')').addClass('dsb_click');
          $(document).find('.'+key+':eq('+i2+')').attr('required', false);
        } else {
          $(document).find('.'+key+':eq('+i2+')').removeClass('dsb_click');
          $(document).find('.'+key+':eq('+i2+')').attr('required', true);
        }
      })
    }
  }
}

function hide_row_tbl(arr, i, doorval, base_url, e) {
  var val = $(e).val();
  if(val == 'Self Assesment') {
    $(e).parent().parent().css('display', 'none');
  }
}

function show_hide_pnmr(arr) {
  // hide tbl
  $(document).find('.'+arr['target']).parent().parent().css('display', 'none');

  // make arr
  var arr1 = $(document).find('.'+arr['from']).val();
  var arr2 = [];
  $.each(arr['cs'], function(k, v) {
    arr2.push(v);
  })
  
  // intersections
  // var arr3 = arr1.filter(e => arr2.includes(e)); // cara 1 ES07
  var arr3 = arr1.filter(e => arr2.indexOf(e) !== -1); // cara 2 ES06
  // var arr3 = arr1.filter(myCallBack); // cara 3
  // function myCallBack(el) {
  //   return arr2.indexOf(el) != -1;
  // }
  
  // show tbl
  if(arr3.length > 0) {
    $(document).find('.'+arr['target']).parent().parent().css('display', 'block');
  }
}

function dsb_jns_kin(arr, i, doorval, base_url, e) {
  var idx = $(e).data('idx');
  $.each(arr['cfg'][idx], function(k, v) {
    if(k == 'enabled') {
      $(e).val(v);
      $(e).trigger('change');
    }
  })
}

function dsb_kin_aksi(arr, i, doorval, base_url, e) {
  // dsb_tbl_aksi
  // dsb_tbl_aksi(e);
}