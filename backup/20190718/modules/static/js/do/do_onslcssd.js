// slcssd-v1
function get_lok_pkt(arr, nowIdx, base_url) {
  	var box = [];
  	var src = $('.'+arr['from']).val();
  	$.each(src, function(key, value) {
  		$(document).find('.'+arr['cs'][value]+':not(:disabled)').each(function(i, e) {
  			var boxdt = $('.'+arr['cs'][value]+':not(:disabled):eq('+i+')').val();
  			box.push(boxdt);
  		})
  	})

  	var res = box;
  	return res;
}

function get_lok_pkt2(arr, nowIdx, base_url) {
	var box = [];
	$.each(arr['from'], function(key, value) {
	  var src = $('.'+arr['from'][key]).val();
	  $.each(src, function(key2, value2) {
		$(document).find('.'+arr['cs'][key][value2]+':not(:disabled)').each(function(i, e) {
		  var boxdt = $('.'+arr['cs'][key][value2]+':not(:disabled):eq('+i+')').val();
		  box.push(boxdt);
		})
	  })  
	})
  
	var res = box;
	return res;
}

function get_kd_akses(arr, nowIdx, base_url) {
  var dbg = $('select.'+arr['trigger']+'[data-idx="'+nowIdx+'"]').val();

  var res = dbg;
  return res;  
}

// ktg pyg
function get_ktg_pyg(arr, e2, base_url) {
	var nibR   = $('#'+arr['trigger']).val().split('-')[0];
	var oss_id = $('#'+arr['trigger']).val().split('-')[1];
	var jiz    = $('#'+arr['trigger']).attr('data-jiz');
  
	var raw = [];
	raw['data'] = { 
					nib : nibR,
					oss_id : oss_id,
					id_jenis_izin : jiz
				  };
	raw['url'] = base_url+"oss_ldap/getDataNIB/";
  
	var res;
	$.ajax({
	  url: raw['url'],
	  dataType: "json",
	  data: raw['data'],
	  type: "POST",
	  success: function(e) {
		res = e.data_investasi;
	  },
	  async: false
	});  
  
	return res;  
}
    
// media pyg
function get_media_pyg(arr, e2, base_url) {
	var nibR   = $('#'+arr['trigger']).val().split('-')[0];
	var oss_id = $('#'+arr['trigger']).val().split('-')[1];
	var jiz    = $('#'+arr['trigger']).attr('data-jiz');
  
	var raw = [];
	raw['data'] = { 
					nib : nibR,
					oss_id : oss_id,
					id_jenis_izin : jiz
				  };
	raw['url'] = base_url+"oss_ldap/getDataNIB/";
  
	var res = [];
	$.ajax({
	  url: raw['url'],
	  dataType: "json",
	  data: raw['data'],
	  type: "POST",
	  success: function(e) {
		res[0] = e.data_investasi;
	  },
	  async: false
	});   
	res[1] = $('.'+arr['trigger2']).val();
  
	return res;  
}

function get_jns_pnmr(arr, nowIdx, base_url) {
  var src   = $('.'+arr['trigger']).val();
  var boxPnmr = [];
  $.each(src, function(k, v) {
    $.each(arr['cs'][v], function(k2, v2) {
      boxPnmr.push(v2);
    })
  })
  var res = boxPnmr;
  return res;  
}

function get_thn_komit(arr, nowIdx, base_url) {
  var res = 0;
  var nowIdxAf = nowIdx - 1;
  if(nowIdx > 1) {
    var res = $('.'+arr['trigger']+':eq('+nowIdxAf+')').val();
	}
	return res;
}

function get_jns_komit(arr, nowIdx, base_url) {
  var res = 0;
	return res;
}

function get_kabkot(arr, nowIdx, base_url) {
  var res = 0;
  return res;  
}

function get_general(arr, nowIdx, base_url) {
  var res = 0;
  return res;  
}
