from flask import Flask, render_template, request, redirect, url_for, jsonify, session
from flask_mysqldb import MySQL
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from datetime import date

import json
import re
import pandas as pd 
import pickle
import MySQLdb as db
import math
import random
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib

import pandas as pd
import pickle
import MySQLdb as db

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'sAw3d@Qq'
app.config['MYSQL_DB'] = 'db_skripsi'

mysql = MySQL(app);

# @app.route('/', methods=['GET', 'POST'])
# def login(task = ''):
# 	today = date.today()
# 	data_db = None
# 	return render_template("layout.html", task='', d='')

@app.route('/')
@app.route('/<task>')
def home(task=''):
	today = date.today()
	data_db = None
	if task == '':
		return render_template("layout.html", task='', d='')
	elif task == 'dashboard':
		dt1 = today.strftime("%Y/%m/%d")
		dt2 = today.strftime("%m")
		dt3 = today.strftime("%Y")
		cur = mysql.connection.cursor()
		cur.execute("SELECT count(id) as jmlh_kalimat from process_kalimat where aktif = 1")
		data_db = cur.fetchall()
		cur.execute("SELECT count(id) as jmlh_kalimat from t_kalimat where aktif = 1 and date(date_tweet) = '" + dt1 + "'")
		data_db2 = cur.fetchall()
		cur.execute("SELECT count(id) as jmlh_kalimat from t_kalimat where aktif = 1 and month(date_tweet) = '" + dt2 + "'")
		data_db3 = cur.fetchall()
		cur.execute("SELECT count(id) as jmlh_kalimat from t_kalimat where aktif = 1 and year(date_tweet) = '" + dt3 + "'")
		data_db4 = cur.fetchall()
		cur.execute("SELECT count(Id) as jmlh_kalimat from process_kalimat where tipe_kalimat = \'1\'")
		data_db5 = cur.fetchall()
		cur.execute("SELECT count(Id) as jmlh_kalimat from process_kalimat where tipe_kalimat = \'0\'")
		data_db6 = cur.fetchall()
		cur.close()
		data_db7 = round(data_db6[0][0] / data_db[0][0] * 100, 1)
		data_db8 = round(data_db5[0][0] / data_db[0][0] * 100, 1)
		return render_template("layout.html", task=task, d=data_db, d2=data_db2, d3=data_db3, d4=data_db4, d5=data_db5,
							   d6=data_db6, d7=data_db7, d8=data_db8)
	elif task == 'dm_kata_baku':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from tb_katadasar where aktif = 1 order by id_ktdasar asc limit 1000")
		data_db = cur.fetchall()
		cur.close()
		return render_template("layout.html", task=task, d=data_db)

	elif task == 'dm_kalimat_hasil':

		cur =mysql.connection.cursor()
		cur.execute("SELECT * from process_kalimat where aktif = 1 order by Id desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
		return render_template("layout.html", task=task, d=data_db)

	elif task == 'dm_kata_negatif':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from m_kata_negatif where aktif = 1 order by id_kata_negatif desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
		return render_template("layout.html", task=task, d=data_db)

	elif task == 'dm_kata_gaul_asing':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from m_kata_gaul_asing where aktif = 1 order by id_kata_gaul asc limit 1000")
		data_db = cur.fetchall()
		cur.close()

		cur =mysql.connection.cursor()
		cur.execute("SELECT id_ktdasar, katadasar from tb_katadasar where aktif = 1 order by id_ktdasar asc limit 1000")
		kata_baku = cur.fetchall()
		cur.close()
		
		return render_template("layout.html", task=task, d=data_db, list=kata_baku)

	elif task == 'input_kalimat':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from t_kalimat where aktif = 1 and hasil > 3.841 order by id desc")
		data_db = cur.fetchall()
		cur.close()
		return render_template("layout.html", task=task, d=data_db)

	# laporan
	elif task == 'lp_kalimat_raw':
		cur =mysql.connection.cursor()
		cur.execute("SELECT * from t_kalimat where aktif = 1 order by id desc limit 1000")
		data_db = cur.fetchall()
		cur.close()
		return render_template("layout.html", task=task, d=data_db)
	elif task == 'lp_kalimat_netpo':
		cur =mysql.connection.cursor()
		cur.close()
		return render_template("layout.html", task=task, d=data_db)

@app.route('/insert', methods=['POST'])
def insert():
	if request.method == "POST":
		param = request.form['param_url']
		if param == "kata_gaul":
			kata_gaul = request.form['kata_gaul']
			kata_asing = request.form['kata_asing']
			id_kata_baku = request.form['id_kata_baku']
			if kata_asing == "" :
				cur = mysql.connection.cursor()
				cur.execute("INSERT INTO m_kata_gaul_asing (kata_gaul, id_kata_baku) VALUES (%s, %s)",
						(kata_gaul, id_kata_baku))
				mysql.connection.commit()
			elif kata_gaul == "" :
				cur = mysql.connection.cursor()
				cur.execute("INSERT INTO m_kata_gaul_asing (kata_asing, id_kata_baku) VALUES (%s, %s)",
							(kata_asing, id_kata_baku))
				mysql.connection.commit()
			task = 'dm_kata_gaul_asing'

		elif param == "kata_negatif":
			kata_negatif = request.form['kata_negatif']
			cur = mysql.connection.cursor()
			cur.execute("INSERT INTO m_kata_negatif (kata_negatif) VALUES (%s)", (kata_negatif))
			mysql.connection.commit()
			task = 'dm_kata_negatif'

		elif param == "kata_baku":
			kata_dasar = request.form['kata_dasar']
			tipe_kata = request.form['tipe_kata']
			cur = mysql.connection.cursor()
			cur.execute("INSERT INTO tb_katadasar (katadasar, tipe_katadasar) VALUES (%s, %s)", (kata_dasar, tipe_kata))
			mysql.connection.commit()
			task = 'dm_kata_baku'

		elif param == "kalimat_hasil":
			preprocessing = request.form['preprocessing']
			tipe_kalimat = request.form['tipe_kalimat']
			cur = mysql.connection.cursor()
			cur.execute("INSERT INTO process_kalimat (preprocessing, tipe_kalimat) VALUES (%s, %s)",
						(preprocessing, tipe_kalimat))
			mysql.connection.commit()
			task = 'dm_kalimat_hasil'

		return redirect(url_for('home', task=task))


@app.route('/del_data', methods=['POST'])
def del_data() :
	if request.method == 'POST' :
		type = request.form['type']
		if type == 'kalimat_hasil' :
			id_data = request.form['id']
			# print(id_data)
			cur = mysql.connection.cursor()
			cur.execute("UPDATE process_kalimat set aktif = 0 where Id ="+id_data)
			# cur.execute("DELETE FROM process_kalimat WHERE Id = "+id_data)
			mysql.connection.commit()
		
		elif type == 'kata_dasar' :
			id_data = request.form['id']
			# print(id_data)
			cur = mysql.connection.cursor()
			cur.execute("UPDATE tb_katadasar set aktif = 0 where id_ktdasar ="+id_data)
			# cur.execute("DELETE FROM tb_katadasar WHERE id_ktdasar = "+id_data)
			mysql.connection.commit()
		
		elif type == 'kata_gaul_asing' :
			id_data = request.form['id']
			# print(id_data)
			cur = mysql.connection.cursor()
			cur.execute("UPDATE m_kata_gaul_asing set aktif = 0 where id_kata_gaul ="+id_data)
			# cur.execute("DELETE FROM tb_katadasar WHERE id_ktdasar = "+id_data)
			mysql.connection.commit()
		
		return jsonify({"status":"OK"})


@app.route('/get_data_by_id', methods=['POST'])
def getDataById() :
	if request.method == 'POST' :
		type = request.form['type']
		if type == 'kata_baku' :
			id_data = request.form['id']
			cur = mysql.connection.cursor()
			cur.execute("SELECT * from tb_katadasar where id_ktdasar="+id_data)
			data = cur.fetchone()
			# print(data)
		
		elif type == 'kata_gaul_asing' :
			id_data = request.form['id']
			cur = mysql.connection.cursor()
			cur.execute("SELECT m_kata_gaul_asing.id_kata_gaul, m_kata_gaul_asing.kata_gaul, m_kata_gaul_asing.kata_asing, m_kata_gaul_asing.date_added, tb_katadasar.katadasar, tb_katadasar.tipe_katadasar, m_kata_gaul_asing.aktif FROM m_kata_gaul_asing INNER JOIN tb_katadasar ON m_kata_gaul_asing.id_kata_baku = tb_katadasar.id_ktdasar WHERE m_kata_gaul_asing.aktif = 1 AND tb_katadasar.aktif = 1 and m_kata_gaul_asing.id_kata_gaul = "+id_data)
			data = cur.fetchone()
			print(data)

			
		return jsonify({"data":data})


@app.route('/update_data', methods=['POST'])
def update_data() :
	if request.method == 'POST' :
		# print(request.form)
		type = request.form['typeData']
		if type == 'kata_baku' :
			id_data = request.form['id']
			kata_dasar = request.form['kata_dasar']
			tipe_kata = request.form['tipe_kata']

			cur = mysql.connection.cursor()
			cur.execute("UPDATE tb_katadasar SET katadasar = '"+kata_dasar+"', tipe_katadasar = '"+tipe_kata+"' where id_ktdasar="+id_data)
			mysql.connection.commit()
			task = 'dm_kata_baku'
		
		elif type == 'kata_gaul_asing' :
			id_data = request.form['id']
			kata_gaul = request.form['kata_gaul']
			kata_asing = request.form['kata_asing']
			id_kata_baku = request.form['id_kata_baku']
			
			cur = mysql.connection.cursor()
			cur.execute("UPDATE m_kata_gaul_asing SET kata_gaul = '"+kata_gaul+"', kata_asing = '"+kata_asing+"' where id_kata_gaul="+id_data)
			mysql.connection.commit()	
			task = 'dm_kata_gaul_asing'

		return redirect(url_for('home', task=task))
			






@app.route('/login', methods=['POST'])
def login():
	if request.method == 'POST' :
		username = request.form['username']
		password = request.form['password']
		cur = mysql.connection.cursor()
		cur.execute("SELECT * from m_user where username='"+username+"' AND password='"+password+"'")
		user = cur.fetchone()
		if(user == None):
			return redirect(url_for('home', task=''))
		else:
			return redirect(url_for('home', task='dashboard'))


@app.route('/logout')
def logout():
	return redirect(url_for('home', task=''))


@app.route('/insert_kalimat', methods=['POST'])
def insert_kalimat():
	if request.method == "POST":
		jsondt = json.dumps(request.form)

		kalimat = request.form['sentence']
		type = request.form['type']
		finalWord = request.form['finalWord']

		cur = mysql.connection.cursor()
		cur.execute("INSERT INTO t_kalimat (tweet, proses, aktif) VALUES (%s, %s, %s)", (kalimat, 1, 1))
		lastid = cur.lastrowid
		mysql.connection.commit()

		cur.execute(
			"INSERT INTO process_kalimat (id_tweet, preprocessing, tipe_kalimat, jsondt) VALUES (%s, %s, %s, %s)",
			(lastid, finalWord, type, jsondt))
		mysql.connection.commit()
		return jsonify({"status": "OK"})


@app.route('/sentence', methods=['POST'])
def sentence():
	if request.method == 'POST':
		data = {}
		sentence = request.form['sentence']

		# kata asli
		data.update({"sentence": sentence})

		# kata lower
		sentenceToLower = sentence.lower()
		data.update({"sentenceToLower": sentenceToLower})

		sentenceDict = sentenceToLower.split("\n")

		# modif
		modifSentence = ''
		arrmodified = []
		for k in sentenceDict:
			modifSentence = re.sub(r"[^a-zA-Z0-9]+", ' ', k)

		data.update({"modifSentence": modifSentence})

		# arrmodifed
		arrmodified = modifSentence.split(' ')
		arrmodified = list(filter(None, arrmodified))
		data.update({"arrmodified": arrmodified})

		# for checkwordsin and stopword
		arrmodified = list(set(arrmodified))
		wordsIn = ','.join('"{0}"'.format(w) for w in arrmodified)

		cur = mysql.connection.cursor()
		cur.execute("SELECT a.stopword FROM stopwords a WHERE a.stopword IN (" + wordsIn + ") ORDER BY a.id")
		data_stop = []
		for sw in cur.fetchall():
			data_stop.append(sw[0])
		cur.close()

		data_stop_word = []
		for val in arrmodified:
			# print(val)
			if val in data_stop:
				data_stop_word.append(val)
		data.update({"data_stop_word": data_stop_word})

		# checkwordin
		cur = mysql.connection.cursor()
		cur.execute(
			"SELECT IFNULL(a.kata_gaul, a.kata_asing) as ex_kata, b.katadasar FROM m_kata_gaul_asing a, tb_katadasar b WHERE a.id_kata_baku=b.id_ktdasar AND a.aktif=1 AND (a.kata_gaul IN (" + wordsIn + ") OR a.kata_asing IN (" + wordsIn + ")) ORDER BY b.id_ktdasar desc")
		data_indb = cur.fetchall()

		data.update({"listFixed": data_indb})

		finalWord = modifSentence
		for i in data_indb:
			finalWord = re.sub(i[0], i[1], finalWord)
		for i in data_stop_word:
			finalWord = re.sub(r'\b' + i + r'\b', '', finalWord)
			# print(finalWord)
		finalWord = re.sub('  ', ' ', finalWord)



		# TODO not a perfect chi square
		fromFinalWord = finalWord.split(' ')
		wordsInFinal = ','.join('"{0}"'.format(w) for w in list(set(fromFinalWord)))
		cur = mysql.connection.cursor()
		cur.execute(
			"SELECT a.kategori, b.kata FROM m_kategori a, t_kategori_kata b WHERE a.id_kategori=b.id_kategori AND b.kata IN (" + wordsInFinal + ")")
		kata_fdb = cur.fetchall()
		data.update({"chisquare": kata_fdb})



		factory = StemmerFactory()
		stemmer = factory.create_stemmer()

		steemWords = []
		for i in fromFinalWord:
			base = stemmer.stem(i)
			if base != i:
				finalWord = re.sub(i, base, finalWord)
				steemWords.append({'inflected': i, 'base': base})

		data.update({"finalWord": finalWord})

		data.update({"steemWords": steemWords})

		cur = mysql.connection.cursor()
		cur.execute("SELECT count(id) as count from t_kalimat")
		data_get = cur.fetchone()
		cur.close()
		calculate = math.ceil((data_get[0] * 10) / 100)

		df_mysql = pd.read_sql('SELECT * FROM process_kalimat limit '+str(calculate)+'', con=mysql.connection)

		df_data = df_mysql[["preprocessing", "tipe_kalimat"]]
		df_x = df_data['preprocessing']
		df_y = df_data.tipe_kalimat
		corpus = df_x
		cv = CountVectorizer()

		X = cv.fit_transform(corpus)  # Fit the Data
		from sklearn.model_selection import train_test_split
		X_train, X_test, y_train, y_test = train_test_split(X, df_y, test_size=0.33, random_state=42)
		# #Naive Bayes Classifier
		from sklearn.naive_bayes import MultinomialNB
		clf = MultinomialNB()
		clf.fit(X_train, y_train)
		# print(clf.fit(X_train, y_train))
		clf.score(X_test, y_test)
		# print(clf.score(X_train, y_train))
		datakalimats = [finalWord]
		vect = cv.transform(datakalimats).toarray()
		# print(vect)
		# print(finalWord)
		my_prediction = clf.predict(vect)
		# print(my_prediction)
		pred = '-'.join(my_prediction)
		# print(my_prediction)
		# print(pred)
		data.update({"my_prediction": pred})
		data.update({"type": pred})

		return jsonify(data)

@app.route('/delete_kalimat_histori', methods = ['POST'])
def delete_kalimat_histori() :
	if request.method == "POST" :
		id_kalimat = request.form['id']
		cur = mysql.connection.cursor()
		cur.execute("UPDATE t_kalimat set aktif = 0 where id ="+id_kalimat)
		mysql.connection.commit()
		return jsonify({"status":"OK"})

@app.route('/get_opt_kata_baku', methods=['POST'])
def get_opt_kata_baku():
	if request.method == "POST":

		term = request.form['search']
		cur = mysql.connection.cursor()
		cur.execute("SELECT * from tb_katadasar where katadasar like \"%"+term+"%\" order by katadasar asc")
		data_db = cur.fetchall()
		cur.close()
		res = []
		array_length = len(data_db)
		print(array_length)
		print(range(array_length))
		for i in range(array_length):
			data = {'id':data_db[i][0], 'text' : data_db[i][1]}
			res.append(data)

		return jsonify(res)

if __name__ == "__main__":
    app.run(debug=True)