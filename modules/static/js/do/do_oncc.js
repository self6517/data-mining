function vld_tgl_ulo(arr, e, base_url) {
  var holidayDates = [];  
  var lok3Arr = [];

  var val  = $(e).data('val'); 
  var val2 = $(e).val();
  if(val2 != '') {
    val = val2;
    // val = '';
  }
  
  $(e).datepicker('destroy');
  $(e).prop("type", "text");
  
  // get holiday
  $.ajax({
    url : base_url+"permohonan_izin/get_holidays",
    dataType: "json",
    data: {},
    type: "POST",
    success: function(data) {
      // prepare holidayDates & lok3Arr
      holidayDates = data;   
      lokasi3group(arr);           

      // datepicker config
      $(e).datepicker({
        dateFormat: 'dd-mm-yy',
        autoclose: 1,

        // min date
        minDate: (function(min) {
          var d   = new Date();     
          var dmy = detect_holiday_weekend(d, holidayDates, lok3Arr, arr, val);

          return dmy;
        })(new Date()),

        // max date
        maxDate: (function(max) {
          var dmy = detect_exp(arr);
          console.log(dmy);
          return dmy;
        })(new Date()),

        // detect holiday and weekend
        beforeShowDay: function(d) {
          var dmy = make_date(d);
          
          // if weekend 
          if([0,6].indexOf(d.getDay()) >= 0) {
            return [false, "","unAvailable"];
          }

          // if holiday & lok3Arr
          if(($.inArray(dmy, holidayDates) != -1) || ($.inArray(dmy, lok3Arr) != -1)) {
            return [false,"","unAvailable"]; 
          } else {
            return [true, "","Available"]; 
          }           
        },
        onSelect: function(dateText, inst) {
          $(e).attr('data-val', dateText);
        }
      }).datepicker('setDate', val);   
    }
  }); 

  // function test(dmy, holidayDates, lok3Arr) {
  //   console.log(dmy);
  //   console.log(holidayDates);
  //   console.log(lok3Arr);
  // }
  
  // SUB FUNCTION //
  function make_date(d) {
    // day
    var day = d.getDate();         
    if(day < 10) { day = "0" + day; }
  
    // month
    var month = d.getMonth()+1;         
    if(month < 10) { month = "0" + month; }
  
    var dmy = month+"-"+day+"-"+d.getFullYear();
    return dmy;
  }
  
  function make_date_1day(d) {
    //add +1 day to the date
    d.setDate(d.getDate() + 1);

    // day
    var day = d.getDate();         
    if(day < 10) { day = "0" + day; }
    
    // month
    var month = d.getMonth()+1;         
    if(month < 10) { month = "0" + month; }
    
    var dmy = month+"-"+day+"-"+d.getFullYear();
    return dmy;
  }

  function make_date_min15day(d) {
    //add +1 day to the date
    d.setDate(d.getDate() - 15);

    // day
    var day = d.getDate();         
    if(day < 10) { day = "0" + day; }
    
    // month
    var month = d.getMonth()+1;         
    if(month < 10) { month = "0" + month; }
    
    var dmy = month+"-"+day+"-"+d.getFullYear();
    return dmy;
  }
  
  function make_date_dmy(d) {
    // day
    var day = d.getDate();         
    if(day < 10) { day = "0" + day; }
  
    // month
    var month = d.getMonth()+1;         
    if(month < 10) { month = "0" + month; }
  
    var dmy = month+"-"+day+"-"+d.getFullYear();
    return dmy;
  }
  
  function lokasi3group(arr) {
    // make arrDate
    var arrDate = []; 
    $.each(arr['target'], function(key, value) {
      var classlength = $(document).find('.'+value).length;
      for(i=0; i<classlength; i++) {
        var valDate = $(document).find('.'+value+':eq('+i+')').attr('data-val');
        if(valDate != '') {
          arrDate.push(valDate);
        }
      }
    })
  
    // group 3
    var occurrences = {};
    for(i=0; i<arrDate.length; i++) {
      occurrences[arrDate[i]] = (occurrences[arrDate[i]] || 0) + 1;
    }
  
    // make lok3Arr
    $.each(occurrences, function(key, value) {
      if(value >= 2) {
        var exp  = key.split('-');
        var keyd = exp[1]+'-'+exp[0]+'-'+exp[2];
  
        lok3Arr.push(keyd);
      }
    })
  }
  
  function detect_holiday_weekend(d, holidayDates, lok3Arr, arr, val) {
    // define
    i = 0;
    param = 5; // 5 hari
  
    // validate holiday, weekend
    while(param != 0) {
      dmy = make_date_1day(d);
      d = new Date(dmy);

      if(($.inArray(dmy, holidayDates) != -1) || ($.inArray(dmy, lok3Arr) != -1) || ([0,6].indexOf(d.getDay()) >= 0)) {
      } else {
        param = param - 1;
      }
      i++;
    }
    return d;
  }

  function detect_exp(arr) {
    var res = '';
    $.ajax({
      url : base_url+"timeline/get_exp/"+arr['id_permohonan'],
      dataType: "json",
      data: {},
      type: "POST",
      success: function(data) {
        var d = new Date(data);
        var dmy = make_date_min15day(d);
        res = new Date(dmy);
      },
      async: false
    })

    return res;
  }

}




