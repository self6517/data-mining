function vld_mekanisme_ulo_20(arr, base_url) {
  var res = [];
  res['status'] = 'success';
  res['msg']    = '';

  var jmlVal  = $(document).find('.'+arr['target']+':not(:disabled) option:selected[value="Pengecekan Lapangan"]').length;
  var jmlMin  = 1;

  if(jmlVal < 1) {
    res['status'] = 'error';
    res['msg']    = 'Pada item mekanisme ulo diharap untuk memilih pengecekan lapangan minimal '+jmlMin+' Lokasi';
    $(document).find('.'+arr['target']+':not(:disabled):first').focus();
  }
  
  return res;
}

function vld_mekanisme_ulo_20_old(arr, base_url) {
  var jmlIdx  = $(document).find('input[name="jmlIdx_'+arr['idx']+'[]"]').val();
  var jmlVal  = $(document).find('.'+arr['target']+':not(:disabled) option:selected[value="Pengecekan Lapangan"]').length;
  var jmlMin  = Math.ceil(jmlIdx * 20 / 100);
  // console.log(jmlIdx, jmlVal, jmlMin);

  var res = [];
  if(jmlVal < jmlMin) {
    res['status'] = 'error';
    res['msg']    = 'Pada item mekanisme ulo diharap untuk memilih pengecekan lapangan minimal '+jmlMin+' Lokasi';
    $(document).find('.'+arr['target']+':not(:disabled):first').focus();
  
  } else {
    res['status'] = 'success';
    res['msg']    = '';
  }
  return res;
}

function vld_mekanisme_ulo_20_old2(arr, base_url) {
  var res = [];
  res['status'] = 'success';
  res['msg']    = '';

  var jnsLyn  = $(document).find('select[name="'+arr['source']+'_num[]"]').val();
  var allowed = [5, 6];
  var cond    = $.inArray(parseInt(jnsLyn), allowed); 
  if(cond == -1) {
    var jmlIdx  = $(document).find('input[name="jmlIdx_'+arr['idx']+'[]"]').val();
    var jmlVal  = $(document).find('.'+arr['target']+':not(:disabled) option:selected[value="Pengecekan Lapangan"]').length;
    var jmlMin  = 1;

    if(jmlVal < 1) {
      res['status'] = 'error';
      res['msg']    = 'Pada item mekanisme ulo diharap untuk memilih pengecekan lapangan minimal '+jmlMin+' Lokasi';
      $(document).find('.'+arr['target']+':not(:disabled):first').focus();
    }
  }
  return res;
}