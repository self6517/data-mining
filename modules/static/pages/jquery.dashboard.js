
/**
* Theme: Zircos  Admin Template
* Author: Coderthemes
* Dashboard
*/

!function($) {
    "use strict";

    var Dashboard1 = function() {
    	this.$realData = []
    };

    //creates Bar chart
    Dashboard1.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barSizeRatio: 0.2,
            barColors: lineColors,
            postUnits: 'k'
        });
    },

    //creates line chart
    Dashboard1.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
        Morris.Line({
          element: element,
          data: data,
          xkey: xkey,
          ykeys: ykeys,
          labels: labels,
          fillOpacity: opacity,
          pointFillColors: Pfillcolor,
          pointStrokeColors: Pstockcolor,
          behaveLikeLine: true,
          gridLineColor: '#eef0f2',
          hideHover: 'auto',
          resize: true, //defaulted to true
          pointSize: 0,
          lineColors: lineColors,
            postUnits: 'k'
        });
    },

    //creates Donut chart
    Dashboard1.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true, //defaulted to true
            colors: colors
        });
    },

    
    Dashboard1.prototype.init = function() {

        //creating bar chart
        var $barData  = [
            { y: '01/19', a: 42 },
            { y: '02/19', a: 75 },
            { y: '03/19', a: 38 },
            { y: '04/19', a: 19 },
            { y: '05/19', a: 93 }
        ];
        this.createBarChart('morris-bar-example', $barData, 'y', ['a'], ['Statistics'], ['#3bafda']);

        //create line chart
        var $data  = [
            { y: '2012', a: 50, b: 0 },
            { y: '2013', a: 75, b: 50 },
            { y: '2014', a: 30, b: 80 },
            { y: '2015', a: 50, b: 50 },
            { y: '2016', a: 75, b: 10 },
            { y: '2017', a: 50, b: 40 },
            { y: '2018', a: 75, b: 50 },
            { y: '2019', a: 100, b: 70 }
          ];
        this.createLineChart('morris-line-example', $data, 'y', ['a','b'], ['Kata Positif','Kata Negatif'],['0.9'],['#ffffff'],['#999999'], ['#10c469','#f5707a']);

        //creating donut chart
        var $donutData = [
                {label: "Kata Negatif", value: 12},
                {label: "Kata Positif", value: 20}
            ];
        this.createDonutChart('morris-donut-example', $donutData, ['#f5707a', "#4bd396"]);
    },
    //init
    $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard1.init();
}(window.jQuery);